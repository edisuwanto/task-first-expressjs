// Import Express.js
const express = require('express');
const bodyParser = require('body-parser');
const ejs = require('ejs');

// Membuat instance aplikasi Express
const app = express();

// Menggunakan EJS sebagai template engine
app.set('view engine', 'ejs');

// Menggunakan middleware bodyParser
app.use(bodyParser.urlencoded({ extended: true }));

// Menyediakan file statis (CSS)
app.use(express.static('public'));

// Array untuk menyimpan daftar tugas
const tasks = [];

// Menampilkan daftar tugas
app.get('/', (req, res) => {
  res.render('index', { tasks });
});

// Menambahkan tugas baru
app.post('/add', (req, res) => {
  const task = req.body.task;
  tasks.push(task);
  res.redirect('/');
});

// Menghapus tugas
app.post('/delete', (req, res) => {
  const taskIndex = req.body.taskIndex;
  tasks.splice(taskIndex, 1);
  res.redirect('/');
});

// Menjalankan aplikasi Express pada port 3000
app.listen(3000, () => {
  console.log('Server berjalan di http://localhost:3000');
});
